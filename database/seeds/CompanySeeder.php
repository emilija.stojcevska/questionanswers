<?php

use Illuminate\Database\Seeder;
use App\Company;
use App\Category;
use App\User;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $company = Company::create([
            'name' => 'company',
        ]);
        if($company){
            Category::create([
                'category' => 'general',
                'company_id' => $company->id
            ]);

            //insert admin
            User::create([
                'name' => 'admin',
                'email' => 'admin@admin.com',
                'password' =>\Hash::make('admin'),
                'is_admin' => 1,
                'company_id' => $company->id,
                'approved' => 1
            ]);
            //insert employee
            for($i = 1; $i<3; $i++){
                $approved = 0;
                if($i == 1){
                    $approved = 1;
                }
                User::create([
                    'name' => 'employee'.$i,
                    'email' => 'employee'.$i.'@test.com',
                    'password' =>\Hash::make('employee'),
                    'company_id' => $company->id,
                    'approved' => $approved
                ]);
            }
        }

    }
}
