# QuestionAnswers

Question answers

1. Run migration - php artisan migrate

2. Run seeder - php db:seed
	- this will create: 
		*company
		*default category
		*admin  - email: admin@admin.com, pass: admin
		*user - approved - email:employee1@test.com, pass: employee
		*user - waiting to be approved - email:employee2@test.com, pass: employee


3. Admin can approve Users or Delete.

4. Admin can create Categories.

5. After user register in the Q&A, he has to wait to be approve by admin.

6. After approving he will recive email, that his account is approve.
