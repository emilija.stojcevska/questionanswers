<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route::get('/', function () {
    return view('welcome');
}); */
Route::get('/', 'Auth\LoginController@showLoginForm')->name('login');
 Route::group(['middleware' => ['is_user_login_active']], function () { 
    //Questions

    Route::get('/questions/{id?}', 'QuestionsController@viewQuestions')->name('viewQuestions');
    Route::get('/my_questions/{id?}', 'QuestionsController@viewMyQuestions')->name('viewMyQuestions');
    Route::get('/create_questions', 'QuestionsController@createQuestions')->name('createQuestions');
    Route::post('/add_questions', 'QuestionsController@addQuestion')->name('addQuestion');
    Route::get('/view_question/${id}', 'QuestionsController@viewQuestion')->name('viewQuestion');
    Route::get('/update_question/{id}', 'QuestionsController@updateQuestion')->name('updateQuestion');
    Route::put('editQuestion', 'QuestionsController@editQuestion')->name('editQuestion');
    Route::delete('deleteQuestion/{id}', 'QuestionsController@deleteQuestion')->name('deleteQuestion');


    //Answer

    Route::post('/add_answer', 'AnswersController@addAnswer')->name('addAnswer');
    Route::post('/edit_answer', 'AnswersController@editAnswer')->name('editAnswer');
    Route::post('/delete_answer', 'AnswersController@deleteAnswer')->name('deleteAnswer');

    //User
    Route::get('viewUsers', 'UsersController@viewUsersFront')->name('viewUsersFront');
});

Route::group(['middleware' => ['is_admin']], function () {

    //Admin 
    Route::get('/view_users', 'AdminController@viewUsers')->name('viewUsers');
    Route::post('/approveEmployee', 'AdminController@approveEmployee')->name('approveEmployee');
    Route::post('removeEmployee', 'AdminController@removeEmployee')->name('removeEmployee');

    Route::get('/viewCategory', 'AdminController@viewCategory')->name('viewCategory');
    Route::get('/create_category', 'AdminController@createCategory')->name('createCategory');
    Route::post('/addCategory', 'AdminController@addCategory')->name('addCategory');
    Route::post('deleteCategory', 'AdminController@deleteCategory')->name('deleteCategory');
});
//Company
Route::get('create_company', 'Auth\RegisterController@register_company')->name('create_company');
Route::post('add_company', 'Auth\RegisterController@add_company')->name('add_company');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
