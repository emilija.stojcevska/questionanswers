<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'companies';

    public function users(){
        return $this->hasMany('App\User')->where('approved', 1);
    }

    public function questions(){
        return $this->hasMany('App\Question');
    }

    public function categories(){
        return $this->hasMany('App\Category');
    }

    protected $fillable = [
        'name',
    ];

}
