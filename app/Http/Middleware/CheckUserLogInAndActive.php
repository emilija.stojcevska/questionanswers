<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckUserLogInAndActive
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      if(!Auth::check()){
           
  
        return redirect('login')->with('success', 'Must be aproved by admin');
       
    }else{
        if (Auth::user()->approved != 1) {
            return redirect('login')->with('success', 'Must be aproved by admin');
        }
    }
        return $next($request);
    }
}
