<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CheckQuestions;
use App\Question;
use App\Category;
use App\Answer;
use Auth;

class QuestionsController extends Controller
{
    public function viewQuestions($id = null){
        
        $company_id = Auth::user()->company_id;
        if($id != null){
            $questions = Question::all()->where('company_id', $company_id)->where('category_id', $id);
        }else{
       

        $questions = Question::all()->where('company_id', $company_id);
        }

        return view('questions.view_questions')->with(['questions'=>$questions]);
    }
    public function viewMyQuestions($id = null){
        
        $company_id = Auth::user()->company_id;
        if($id != null){
            $questions = Question::all()->where('company_id', $company_id)->where('category_id', $id)->where('user_id', Auth::id());
        }else{
       

        $questions = Question::all()->where('company_id', $company_id)->where('user_id', Auth::id());
        }

        return view('questions.my_questions')->with(['questions'=>$questions]);
    }


    public function createQuestions(){

        $categories = Category::all()->where('company_id', Auth::user()->company_id);

        return view('questions.create_question')->with(['categories' => $categories]);
    }


    public function addQuestion(CheckQuestions $request){
       
        $question = Question::create([
            'question' => $request->question,
            'user_id' => Auth::id(),
            'company_id' => Auth::user()->company_id,
            'category_id' => $request->category
        ]);

        if($question){
            return redirect()->route('viewQuestions')->with('success', 'Question add successfully');
        }
    }


    public function viewQuestion($id){
        $question = Question::findOrFail($id);

        return view('questions.view')->with(['question' => $question]);
    }

    public function deleteQuestion($id){
        $question = Question::findOrFail($id)->delete();

        if($question){
            Answer::where('question_id', $id)->delete();
            return redirect()->route('viewQuestions')->with('success', 'Question deleted successfully');
        }
    }

    public function updateQuestion($id){
        $question = Question::findOrFail($id);

        return view('questions.update_question')->with(['question' => $question]);
    }

    public function editQuestion(CheckQuestions $request){
        $question = Question::findOrFail($request->question_id);
        $question->update([
            'question' => $request->question,
            'category_id' => $request->category
        ]);

        if($question){
            return redirect()->back()->with('success', 'Question updated successfully');
        }
    }
}
