<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

class UsersController extends Controller
{
    public function viewUsersFront(){
        $company_id = Auth::user()->company_id;
        $users = User::all()->where('company_id', $company_id)->where('approved', 1);

        return view('viewUsers')->with(['users'=>$users]);
    }
}
