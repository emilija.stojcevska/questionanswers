<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Company;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use App\Category;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        
        $is_admin = 0;
        $approved = 0;
        if(isset($data['is_admin'])){
            $is_admin = $data['is_admin'];
            $approved = $data['approved'];
        }
       
        if($data)
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'is_admin' => $is_admin,
            'company_id' => $data['company_id'],
            'approved' => $approved
            
        ]);
    }

    public function register_company(){
        return view('auth.register_company');
    }

    protected function add_company(Request $request){
        $validatedData = $request->validate( [
            'company_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);
        $company = new Company;
        $company = Company::create([
            'name' => $request->company_name
        ]);
           
        if($company){
            Category::create([
                'category'=>'general',
                'company_id' => $company->id
            ]);
            $data = $request->toArray();
            $data['company_id'] = $company->id;
            $data['is_admin'] = 1;
            $data['approved'] = 1;
            $user = $this->create($data);
        }

       
        if($user){
            $this->guard()->login($user);
            return  redirect($this->redirectPath());
            
        }
       
    }

    public function showRegistrationForm()
    {
        $companies = Company::all();
        $data = [
            'companies' => $companies
        ];

        return view('auth.register')->with($data);
    }


    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));
        
        
        return redirect()->back()->with(['success'=>'You register successfully, you can log in after your status is approved']);
    }
}
