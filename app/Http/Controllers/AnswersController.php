<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CheckAnswers;

use App\Answer;
use Auth;

class AnswersController extends Controller
{
    public function addAnswer(CheckAnswers $request){
        $answer = Answer::create([
            'answer' => $request->answer,
            'question_id' => $request->id,
            'user_id' => Auth::id()
        ]);

        $data = [
            'status'=>'updated',
             'answer' => $answer->answer, 
             'answer_id' => $answer->id,
             'user' => $answer->user->name
        ];
       
       if($answer){
            return response()->json($data);
        } 
    }

    public function editAnswer(CheckAnswers $request){
        $answer = Answer::findOrFail($request->id);
        if($answer){
            $answer->update([
                'answer' => $request->answer,
            ]);

            $data = [
                'status'=>'updated',
                 'answer' => $answer->answer, 
                 'answer_id' => $answer->id,
                
            ];
           
            return response()->json($data);

        }
    }

    public function deleteAnswer(Request $request){
        $answer = Answer::findOrFail($request->id)->delete();
        if($answer){
            
            return response()->json(['status'=>'deleted']);

        }

    }
}
