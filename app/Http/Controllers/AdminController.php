<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CheckCategory;
use App\User;
use App\Category;
use App\Question;
use Mail;
use App\Mail\ApproveUserMail;



use Auth;

class AdminController extends Controller
{
    public function viewUsers(){

        $company_id = Auth::user()->company_id;
        $users = User::all()->where('company_id', $company_id)->except(Auth::id());
       
        return view('admin.view_users')->with(['users'=>$users]);
    }

    public function approveEmployee(Request $request){
        $user = User::find($request->id);
        if($user){
            $user->update([
                'approved' => 1
            ]);
            Mail::to($user->email)->send(new ApproveUserMail($user));

            return response()->json(['status'=>'updated']);
        }else{

        }
    }

    public function removeEmployee(Request $request){
        $user = User::findOrFail($request->id)->delete();
        if($user){
           
            return response()->json(['status'=>'deleted']);
        }else{

        }
    }


    public function viewCategory(){
        $categories = Category::all()->where('company_id', Auth::user()->company_id);
        return view('admin.view_categories')->with(['categories'=>$categories]);
    }
    public function deleteCategory(Request $request){
        $category = Category::findOrFail($request->id)->delete();
        if($category){
           
            return response()->json(['status'=>'deleted']);
        }else{

        }
    }


    public function createCategory(){
        return view('admin.create_category');
    }

    public function addCategory(CheckCategory $request){
        $categoryNew = strtolower($request->category);
        $categoryExist = Category::where('category', $categoryNew)->where('company_id', Auth::user()->company_id)->get();
      
        if(count($categoryExist) <= 0){
        $category = Category::create(
            [
                'category' => $categoryNew,
                'company_id' =>  Auth::user()->company_id
            ]
            );

            return back()->with('success','Category was created');
        }else{
            return back()->with('error', 'Category exist');
        }
    }
}
