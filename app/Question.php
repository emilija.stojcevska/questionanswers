<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table = 'questions';

    protected $fillable = [
        'question', 'company_id', 'user_id', 'category_id'
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function answers(){
        return $this->hasMany('App\Answer');
    }

    public function category(){
        return $this->belongsTo('App\Category');
    }

    public function company(){
        return $this->belongsTo('App\Company');
    }



}
