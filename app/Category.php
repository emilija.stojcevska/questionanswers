<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';


    public function questions(){
        return $this->hasMany('App\Question');
    }

    public function company(){
        return $this->belongsTo('App\Company');
    }

    protected $fillable = [
        'category', 'company_id'
    ];
}
