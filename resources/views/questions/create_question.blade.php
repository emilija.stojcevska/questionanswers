@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <div class="card-header">Create Question</div>
                @if(count($errors->all())>0)
                <div class="alert alert-danger" id="success" role="alert">
                    @foreach ($errors->all() as $message) 
                        {{$message}}
                    @endforeach
                    
                </div>
                @endif
                <div class="card-body">
                    <form action={{route('addQuestion')}} method="POST">
                        @csrf

                        <div class="form-group">
                            <label for="question">Question</label>
                            <input type="text" class="form-control" id="question" placeholder="Enter your question" name="question">
                            
                        </div>

                        <div class="form-group">
                            <label for="category">Select category</label>
                            <select class="form-control" id="category" name="category">
                                @foreach ($categories as $category)
                                    <option value="{{$category->id}}">{{$category->category}}</option>
                                @endforeach
                            
                            
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary">Create</button>

                    </form>
                </div>
                
            </div>
        </div>
    </div>
</div>
@endsection
