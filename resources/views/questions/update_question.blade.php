@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Update Question</div>
                <div class="card-body">
                    @if(Session::has('success'))
                    <div class="alert alert-success" id="success" role="alert">
                        {{Session::get('success')}}
                    </div>
                @endif
                    @if(Session::has('error'))
                    <div class="alert alert-danger" id="success" role="alert">
                        {{Session::get('error')}}
                    </div>
                @endif
                    @if(count($errors->all())>0)
                    <div class="alert alert-danger" id="success" role="alert">
                        @foreach ($errors->all() as $message) 
                            {{$message}}
                        @endforeach
                        
                    </div>
                    @endif
                
                    <form action={{route('editQuestion')}} method="POST">
                        @csrf
                        <input type="hidden" name="_method" value="put" />
                        <input type="hidden" name="question_id" value="{{$question->id}}" />
                        <div class="form-group">
                            <label for="question">Question</label>
                            <input type="text" value="{{$question->question}}"class="form-control" id="question" placeholder="Enter your question" name="question">
                            
                        </div>

                        <div class="form-group">
                            <label for="category">Select category</label>
                            <select class="form-control" id="category" name="category">
                                @foreach ($question->company->categories as $category)
                                    <option @if($question->category_id == $category->id) selected @endif value="{{$category->id}}">{{$category->category}}</option>
                                @endforeach
                            
                            
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary">Update</button>

                    </form>
                </div>
                
            </div>
        </div>
    </div>
</div>
@endsection
