@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
       
            @if(Session::has('success'))
                <div class="alert alert-success" id="success" role="alert">
                    {{Session::get('success')}}
                </div>
            @endif
            @if(count($errors->all())>0)
            <div class="alert alert-danger" id="success" role="alert">
                @foreach ($errors->all() as $message) 
                    {{$message}}
                @endforeach
                
            </div>
            @endif
            
               
            <div class="col-md-8 bgWhite marginBottom2">
                <div class="card-body">
                <h5 class="card-title">{{$question->question}}
                   
                    <span class="float-right">Answers <span id="countAnswers"class="badge badge-primary">{{count($question->answers)}}</span></span>
                
                </h5>
                <div class="displayFlex">
                    <div >
                    <cite title="Source Title"> By {{$question->user->name}}</cite> <br>
                    <a href="{{route('viewQuestions', $question->category->id) }}"><span class="badge badge-pill badge-secondary">{{$question->category->category}}</span></a>
                    </div>
                    
                    @if($question->user->id == Auth::id())
                        

                        <form action="{{route('deleteQuestion', $question->id)}}" method="post">
                            <a href="{{route('updateQuestion', $question->id)}}" class="btn btn-primary btn-sm">Update</a>
                            <input class="btn btn-danger btn-sm" type="submit" value="Delete" />
                            <input type="hidden" name="_method" value="delete" />
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        </form>
                      
                    @endif
                   

                </div>
                </div>
        </div>

        <div class="col-md-8">
                    <form>
                        <div class="form-group">
                            <label for="answer">Answers</label>
                            <textarea class="form-control" id="answer" rows="3" name="answer"></textarea>
                          </div>
                          <button class="btn btn-primary float-right" onclick="addQuestion({{$question->id}}, this, event)">
                            Create
                          </button>
                    </form>
                </div>

                
                    <div class="answers col-md-8 marginTop2">
                        @foreach ($question->answers as $answer)
                            <div class="answer bgWhite marginBottom2"> 
                                <div class="answerBody"> {{$answer->answer}}</div>
                                <cite title="Source Title" class="text-left"> By {{$answer->user->name}}</cite>
                               @if($answer->user->id == Auth::id())
                               <div class="text-right">
                               
                                <button class="btn btn-primary btn-sm" onclick="editAnswer({{$answer->id}}, this, event)">
                                    Edit
                                </button>
                                <button class="btn btn-danger btn-sm" onclick="deleteAnswer({{$answer->id}}, this, event)">
                                    Delete
                                </button>
                            </div>
                                @endif
                            
                            </div>
                        @endforeach
                        
                    </div>
              
       
    </div>
</div>
@endsection

<script>
function addQuestion(id, el, event){
    event.preventDefault();
    var answer = document.getElementById("answer").value;
    
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "{{route('addAnswer')}}", false);
    var data = {
        'id': id,
        'answer' : answer
        }; 
        xhttp.setRequestHeader("content-type", "application/json");
        xhttp.setRequestHeader("X-CSRF-TOKEN", "{{ csrf_token() }}");
        xhttp.onload = function () {
                var response = xhttp.responseText;
                if (xhttp.readyState == 4 && xhttp.status == "200") {
                   response = JSON.parse(response);
                   var textArea = document.getElementById("answer");
                   textArea.value="";
                    var answer = document.createElement('div'); 
                    var countAnswers = document.getElementById('countAnswers').innerHTML;
                    var i = parseInt(countAnswers)+1;
                    
                    document.getElementById('countAnswers').innerHTML = i;
                    answer.classList.add('answer');
                    answer.innerHTML = `
                                <div class="answerBody">${response.answer}</div> 
                                <cite title="Source Title" class="text-left"> By ${response.user}</cite>
                                <div class="text-right">
                                  
                                <button class="btn btn-primary btn-sm" onclick="editAnswer(${response.answer_id}, this, event)">
                                    Edit
                                 </button>
                                <button class="btn btn-danger btn-sm" onclick="deleteAnswer(${response.answer_id}, this, event)">
                                    Delete
                                 </button>
                                 </div>
                    `
                     var divAnswers = document.getElementsByClassName('answers')[0];
                     divAnswers.prepend(answer);
                        
                } else {
                        /* document.getElementById("app").innerHTML = response; */  
                }
        }
        xhttp.send(JSON.stringify(data));
   /*  console.log(data); */
}


function editAnswer(answer_id, el, event){
    
  
    var textarea = document.createElement('textarea'); 
    textarea.classList.add('form-control');
    textarea.innerHTML = `${el.parentElement.parentElement.getElementsByClassName('answerBody')[0].innerHTML}`;
    el.parentElement.parentElement.getElementsByClassName('answerBody')[0].replaceWith(textarea);
    textarea.focus();
    
    el.innerHTML = 'Save';
    el.onclick = function() { saveAnswer(answer_id, this); }
    

}

function saveAnswer(id, el){
   
    var element = el.parentElement.parentElement.getElementsByTagName('textarea')[0].value;
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "{{route('editAnswer')}}", false);
    var data = {
        'id': id,
        'answer' : element
        }; 
        xhttp.setRequestHeader("content-type", "application/json");
        xhttp.setRequestHeader("X-CSRF-TOKEN", "{{ csrf_token() }}");
        xhttp.onload = function () {
                var response = xhttp.responseText;
                if (xhttp.readyState == 4 && xhttp.status == "200") {
                   response = JSON.parse(response);
                   el.innerHTML = 'Edit';
                   var div = document.createElement('div'); 
                   div.classList.add("answerBody")
                   div.innerHTML = response.answer;
                   el.parentElement.parentElement.getElementsByTagName('textarea')[0].replaceWith(div);
                   el.onclick = function() { editAnswer(response.answer_id, this, event) }      
                } else {
                        /* document.getElementById("app").innerHTML = response; */  
                }
        }
        xhttp.send(JSON.stringify(data));
}

function deleteAnswer(id, el, event){
    var x = confirm("Are you sure you want to delete?");
  if (x){
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "{{route('deleteAnswer')}}", false);
    var data = {
        'id': id,
        }; 
        xhttp.setRequestHeader("content-type", "application/json");
        xhttp.setRequestHeader("X-CSRF-TOKEN", "{{ csrf_token() }}");
        xhttp.onload = function () {
                var response = xhttp.responseText;
                if (xhttp.readyState == 4 && xhttp.status == "200") {
                    var countAnswers = document.getElementById('countAnswers').innerHTML;
                    var i = parseInt(countAnswers)-1;
                    
                    document.getElementById('countAnswers').innerHTML = i;
                    el.parentNode.parentNode.parentNode.removeChild(el.parentNode.parentNode);     
                } else {
                        /* document.getElementById("app").innerHTML = response; */  
                }
        }
        xhttp.send(JSON.stringify(data));
    }else{
        return false;
    }

}
</script>
