@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        
            @if(Session::has('success'))
            <div class="col-md-8">
                <div class="alert alert-success" id="success" role="alert">
                    {{Session::get('success')}}
                </div>
            </div>
            @endif
            @if(count($questions)<=0)
            <div class="col-md-8">
                <h3>No questions</h3>
                <a href="{{route('createQuestions')}}"> Create Question </a>
            </div>
            @else
            <div class="col-md-8">
                <a href="{{route('createQuestions')}}" class="float-right"> Create Question </a>
            </div>
                @foreach ($questions as $question)
                
                <div class="col-md-8 bgWhite marginBottom2">
                        <div class="card-body">
                        <h5 class="card-title">{{$question->question}}
                           
                            <span class="float-right">Answers <span class="badge badge-primary">{{count($question->answers)}}</span></span>
                        
                        </h5>
                        <div class="displayFlex">
                            <div >
                            <cite title="Source Title"> By {{$question->user->name}}</cite> <br>
                            <a href="{{route('viewQuestions', $question->category->id) }}"><span class="badge badge-pill badge-secondary">{{$question->category->category}}</span></a>
                            </div>
                           
                            <a href="{{route('viewQuestion', $question->id)}}" class="btn btn-primary float-right">View</a>

                        </div>
                        </div>
                </div>
                @endforeach
                @endif
          
        
    </div>
</div>
@endsection
 