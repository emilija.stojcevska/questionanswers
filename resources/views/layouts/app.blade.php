<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/main.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand text-capitalize" href="{{ url('/questions') }}">
                @if(Auth::check())
                {{ Auth::user()->company->name }}
                @else
                    
                Q&A

                @endif
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
          
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav mr-auto">
                @if(Auth::check())
                <a class="nav-link {{ request()->routeIs('viewQuestions') ? 'active' : '' }}" href="{{route('viewQuestions')}}">
                    Questions <span class="badge badge-primary"> {{ count(Auth::user()->company->questions) }} </span>
                </a>
                <a class="nav-link {{ request()->routeIs('viewUsersFront') ? 'active' : '' }}" href="{{route('viewUsersFront')}}">
                    Users <span class="badge badge-primary">{{ count(Auth::user()->company->users) }}</span>
                </a>
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Categories  <span class="badge badge-primary">{{count(Auth::user()->company->categories)}}</span>
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    @foreach (Auth::user()->company->categories as $category)
                    <a class="dropdown-item" href="{{route('viewQuestions', $category->id)}}">{{$category->category}}</a>
                        @endforeach
                  </div>
                </li>
                @if(Auth::user()->is_admin == 1) 
                <a class="nav-link {{ request()->routeIs('viewUsers') ? 'active' : '' }}" href="{{route('viewUsers')}}">
                    Menage Users
                </a>
                <a class="nav-link {{ request()->routeIs('viewCategory') ? 'active' : '' }}" href="{{route('viewCategory')}}">
                    Menage Category
                </a>
                @endif
                @endif
              </ul>
              <!-- Right Side Of Navbar -->
              <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('create_company') }}">{{ __('Register company') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('viewMyQuestions') }}">
                                {{ __('My Questions') }}
                            </a>
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
            </div>
          </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
