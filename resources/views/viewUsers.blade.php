@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                <table class="table table-responsive-sm table-responsive-md	">
                    <thead>
                      <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Questions ask</th>
                        <th scope="col">Answers</th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                      <tr>
                      <th scope="row" class="text-capitalize">{{$user->name}}</th>
                        <td>{{$user->email}}</td>
                        <td>
                           {{count($user->questions)}}
                        </td>
                        <td>
                            {{count($user->answers)}}
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
            </div>
        </div>
    </div>
</div>
@endsection

