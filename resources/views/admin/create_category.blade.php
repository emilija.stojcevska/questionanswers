@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
               
          
                <div class="card-header">Create Category</div>
                @if(Session::has('success'))
                    <div class="alert alert-success" id="success" role="alert">
                        {{Session::get('success')}}
                    </div>
                @endif
                @if(Session::has('error'))
                    <div class="alert alert-danger" id="success" role="alert">
                        {{Session::get('error')}}
                    </div>
                 @endif
                <div class="card-body">
                    <form action={{route('addCategory')}} method="POST">
                        @csrf

                        <div class="form-group">
                            <label for="category">Category</label>
                            <input type="text" class="form-control" id="category" placeholder="Enter your category" name="category">
                            
                        </div>

                       
                        <button type="submit" class="btn btn-primary">Create</button>

                    </form>
                </div>
                
            </div>
        </div>
    </div>
</div>
@endsection
