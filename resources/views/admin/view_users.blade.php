@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
              @if(count($users)>0)
                <table class="table table-responsive-sm table-responsive-md	">
                    <thead>
                      <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Approve</th>
                        <th scope="col">Delete</th>
                      </tr>
                    </thead>
                  
                    <tbody>
                        @foreach($users as $user)
                      <tr>
                      <th scope="row" class="text-capitalize">{{$user->name}}</th>
                        <td>{{$user->email}}</td>
                        <td>
                            @if($user->approved == 1)
                            Approved
                            @else
                            <button type="button" class="btn btn-primary approved " data-update="{{$user->id}}"  onclick="approveEmployee({{$user->id}}, this)">Approve</button>
                            @endif
                        </td>
                        <td>
                            <button type="button" class="btn btn-danger"  data-delete="{{$user->id}}" onclick="ConfirmDelete({{$user->id}}, this)">Delete</button>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  
                  </table>
                  @else
                  <h3 class="text-center"> No users </h3>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection

<script>

/* document.getElementsByClassName("approved").addEventListener("click", approveEmployee);
 */
function approveEmployee(id, el){
   
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "{{route('approveEmployee')}}", false);
    var data = {
        'id': id,
        
        }; 
        xhttp.setRequestHeader("content-type", "application/json");
        xhttp.setRequestHeader("X-CSRF-TOKEN", "{{ csrf_token() }}");
        xhttp.onload = function () {
                var response = xhttp.responseText;
                if (xhttp.readyState == 4 && xhttp.status == "200") {
                   
                            el.innerHTML = "Approved";
                           el.disabled = true;

                     
                        
                } else {
                        /* document.getElementById("app").innerHTML = response; */  
                }
        }
        xhttp.send(JSON.stringify(data));
   /*  console.log(data); */
}


function ConfirmDelete(id, el)
{
  var x = confirm("Are you sure you want to delete?");
  if (x){
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "{{route('removeEmployee')}}", false);
    var data = {
        'id': id,
        
        }; 
        xhttp.setRequestHeader("content-type", "application/json");
        xhttp.setRequestHeader("X-CSRF-TOKEN", "{{ csrf_token() }}");
        xhttp.onload = function () {
                var response = xhttp.responseText;
                if (xhttp.readyState == 4 && xhttp.status == "200") {
                   
                            el.parentNode.parentNode.parentNode.removeChild(el.parentNode.parentNode);
 
                } else {
                         
                }
        }
        xhttp.send(JSON.stringify(data));

  }
     
  else{
    return false;
  }
}



</script>