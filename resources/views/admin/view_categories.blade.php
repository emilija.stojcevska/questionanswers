@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                @if(count($categories)<=0)
                    No categories
                <a  href="{{route('createCategory')}}" class="createCategory"> Create Category </a>

                @else
                <a href="{{route('createCategory')}}" class="createCategory"> Create Category </a>
                <table class="table">
                    <thead>
                      <tr>
                        <th scope="col">Category</th>
                        
                        <th scope="col">Delete</th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach($categories as $category)
                      <tr>
                        <td scope="row">{{$category->category}}</td>
                        <td>
                            <button type="button" class="btn btn-danger"  data-delete="{{$category->id}}" onclick="ConfirmDelete({{$category->id}}, this)">
                                Delete
                            </button>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                  @endif
            </div>
        </div>
    </div>
</div>
@endsection

<script>

/* document.getElementsByClassName("approved").addEventListener("click", approveEmployee);
 */



function ConfirmDelete(id, el)
{
  var x = confirm("Are you sure you want to delete?");
  if (x){
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "{{route('deleteCategory')}}", false);
    var data = {
        'id': id,
        
        }; 
        xhttp.setRequestHeader("content-type", "application/json");
        xhttp.setRequestHeader("X-CSRF-TOKEN", "{{ csrf_token() }}");
        xhttp.onload = function () {
                var response = xhttp.responseText;
                console.log(response);
                if (xhttp.readyState == 4 && xhttp.status == "200") {
                   
                  el.parentNode.parentNode.parentNode.removeChild(el.parentNode.parentNode);
 
 
                } else {
                         
                }
        }
        xhttp.send(JSON.stringify(data));

  }
     
  else{
    return false;
  }
}



</script>